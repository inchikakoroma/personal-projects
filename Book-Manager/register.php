<?php 

include("header.php");

// set default date
date_default_timezone_set("Australia/Brisbane");



?>


<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Register</div>
                <div class="panel-body">
                <form role="form" id="form" method="POST" action="routes/submit-register.php">
                    <div class="form-group">
                        <label for"firstName">First Name:</label>
                        <input type="text" name="firstName" class="form-control" id="firstName"></input>
                        <label id="firstError"  style="display: none;">You Must Enter Your First Name</label>
                    </div>

                    <div class="form-group">
                        <label for"lastName">Last Name:</label>
                        <input type="text" name="lastName" class="form-control" id="lastName"></input>
                        <label id="lastError"  style="display: none;">You Must Enter Your Last Name</label>
                    </div>

                    <div class="form-group">
                        <label for"email">Email:</label>
                        <input type="email" name="email" class="form-control" id="email"></input>
                        <label id="emailError" style="display: none;">You Must Enter A Valid Email</label>
                    </div>

                    <div class="form-group">
                            <label for="branch">Branch</label>

                          
                                <select id="branch" type="select" class="form-control" name="branchID" value="">
                                    <option value="">Select One</option>
                                    
                                    <option value="Database">Database</option> 
                                </select>                              
                                <span class="help-block" style="display:none;" id="branchError">You Must Choose A Branch</span>
            

                    </div>

                    <div id="branch2" style="display:none;" class="form-group">
                            <label for="otherBranch">Other Branch</label>

                            <div class="col-md-6">
                                <input id="otherBranch" type="text" class="form-control" name="otherBranch">
                                <span class="help-block" style="display:none;" id="otherBranchError">Please Enter Your Branch</span>
                            </div>
                    </div>

                    <div class="form-group">
                        <label for"password">Password:</label>
                        <input type="password" name="password" class="form-control" id="password"></input>
                        <label id="passwordError" style="display: none;">You Must Enter A Password Longer than 8 Characters</label>
                    </div>

                    <div class="form-group">
                            <label for="branch">Type</label>

                          
                                <select id="branch" type="select" class="form-control" name="branchID" value="">
                                    <option value="">Select One</option>
                                    
                                    <option value="Database">Database</option> 
                                </select>                              
                                <span class="help-block" style="display:none;" id="branchError">You Must Choose A Branch</span>

                    </div>

                    <div class="form-group">
                        <label for"phoneNumber">Phone Number:</label>
                        <input type="text" name="phoneNumber" class="form-control" id="phoneNumber"></input>
                        <label id="phoneError" style="display: none;">You Must Enter A Valid Phone Number</label>
                    </div>

                    <div class="form-group">
                        
                        Date Of Birth: </br><input type="text" name="dateOfBirth" class"form-control" id="dob" required></input>
                        <label id="dateError" style="display: none;">You Must Enter Your Date Of Birth</label>
                    
                    </div>

                    <div class="form-group">
                        <input type="submit" id="submitButton" name="mySubmit" class="btn btn-success" value="Submit">
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>



<script type="text/javascript">

    $(document).ready(function(){

        // init date of birth datepicker 
        $("#dob").datepicker();

        // First Name
        $("#firstName").on("blur", function(){

            var firstName = $("#firstName").val();

            if(firstName == ""){
                // show errors
                showErrorText("#firstError");
                showErrorColor("#firstName");
            }else{
                // hide errors
                hideErrorText("#firstError");
                hideErrorColor("#firstName");
            }

        });

        // Last Name
        $("#lastName").on("blur", function(){

            var lastName = $("#lastName").val();

            if(lastName == ""){
                // show errors
                showErrorText("#lastError");
                showErrorColor("#lastName");
            }else{
                // hide errors
                hideErrorText("#lastError");
                hideErrorColor("#lastName");
            }

        });

        // Email
        $("#email").on("blur", function(){

            var email = $("#email").val();

            var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
            
            if(!testEmail.test(this.value) || this.value == ""){
                // show errors
                showErrorText("#emailError");
                showErrorColor("#email");
            }else{
                // hide errors
                hideErrorText("#emailError");
                hideErrorColor("#email");
                
            }
        });

        // Password
        $("#password").on("blur", function(){

            var password = $("#password").val();

            if(this.value == "" || this.value.length < 8){
                // show errors
                showErrorText("#passwordError");
                showErrorColor("#password");
            }else{
                // hide errors
                hideErrorText("#passwordError");
                hideErrorColor("#password");
                
            }
           
        });

        
        // Ticket Number Error
        $("#ticketNumber").on("blur", function(){

           var ticketNum = $("#ticketNumber").val();

           if(!ticketNum.match(/^\d+$/)) {
                // show errors 
                showErrorText("#ticketError");
                showErrorColor("#ticketNumber");     
            }else{
                // hide errors
                hideErrorText("#ticketError");
                hideErrorColor("#ticketNumber");

            }
        });

        // Phone Number Error
        $("#phoneNumber").on("blur", function(){

           var phoneNum = $("#phoneNumber").val();

           if(!phoneNum.match(/^\d+$/)) {
                // show errors 
                showErrorText("#phoneError");
                showErrorColor("#phoneNumber");     
            }else{
                // hide errors
                hideErrorText("#phoneError");
                hideErrorColor("#phoneNumber");

            }
        });

        // when the from is submited encrypt the password
        $("form").submit(function(){
           // Let's find the input to check
           var $input = $(this).find("input[name=password]");
           if ($input.val()) {
            // change the set password to an encrypted password
             var pass2 = encrypt("#password");
             $input.val(pass2);
           }

        });

    });

    // Error Functions

    function showErrorText(id){
        // show error text
        $(id).css("display","inline");
        $("#submitButton").prop('disabled', true);
    }

    function showErrorColor(id){
        // color border color
        $(id).css("border-color", "red");
    }

    function hideErrorText(id){
        // hide errors
        $(id).css("display","none");
        $("#submitButton").prop('disabled', false);
    }

    function hideErrorColor(id){
        // hide errors
        $(id).css("border-color", "#66afe9");
    }


</script>





