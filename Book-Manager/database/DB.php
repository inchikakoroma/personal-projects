<?php

    class Db {
        
        // The database connection
        protected static $connection;

        /**
         * Connect to the database
         * 
         * @return bool false on failure / mysqli MySQLi object instance on success
         */
        
        public function connect() {    
            
            // Try and connect to the database
            if(!isset(self::$connection)) {

                // Load configuration as an array. Use the actual location of your configuration file
                $config = parse_ini_file('./config.ini'); 
                self::$connection = new mysqli($config['DB_HOST'],$config['DB_USERNAME'],$config['DB_PASSWORD'],$config['DB_DATABASE']);
                

            }

            // If connection was not successful, handle the error
            if(self::$connection === false) {
               
                // Handle error - notify administrator, log to a file, show an error screen, etc.
                return false;
            }

            return self::$connection;

        }

        /**
         * Query the database
         *
         * @param $query The query string
         * @return mixed The result of the mysqli::query() function
         */
        public function query($query) {
            
            // Connect to the database
            $connection = $this->connect();

            // Query the database
            $result = $connection->query($query);

            if (!$result){
                //die('Insert Error: ' . mysql_error());
                die($connection->error);
            }

            return $result;

            $connection->close();

        }

        /**
         * Fetch rows from the database (SELECT query)
         *
         * @param $query The query string
         * @return bool False on failure / array Database rows on success
         */
        public function select($query){

            $results = $this->query($query);

            // check for results
            if($results->num_rows > 0){

                while ($row = $results->fetch_assoc()){

                    $rows[] = $row;

                }

                return $rows;

            }

            
        }

        /**
         * Fetch first from the database (SELECT query)
         *
         * @param $query The query string
         * @return bool False on failure / array Database rows on success
         */
        public function selectFirst($query){

            $result = $this->query($query);

            if($result === false) {
                return false;
            }

            $row = mysqli_fetch_array($result);


            return $row;

        }

        /**
         * Fetch the last error from the database
         * 
         * @return string Database error message
         */
        public function error(){

            $connection = $this->connect();

            return $connection->error;

        }


    }