<?php 

include("header.php");

?>

<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Login Form</div>
				<div class="panel-body">
					<form role="form" id="login-form" method="POST" action="routes/post-login.php">
						<div class="form-group">
							<label for="username">Email: </label>
							<input type="text" name="email" class="form-control" id="email" required></input>
							<div style="display:none;" id="emailError">You Must Enter A Valid Email Address</div>
						</div>
						<div class="form-group">
							<label for="password">Password</label>
							<input type="password" name="password" class="form-control" id="password" required></input>
							<div style="display:none;" id="passwordError">Your Password Must Be Longer Than 8 Characters</div>
						</div>
						<div class="form-group">
							<input type="submit" name="loginForm" class="btn btn-success" value="Login" id="loginButton"></input>
						</div>
					</form>
				</div>
			</div>
			<div class="form-group">
				<a href="register.php" class="btn btn-link" id="registerButton">Register</a>
			</div>
		</div>
	</div>
</div>
