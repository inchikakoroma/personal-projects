<?php

namespace App\Http\Controllers\Reg;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\AuthTraits\RedirectsUsers;
use App\User;
use Validator;
use Input;
use Image;

class RegistrationController extends Controller
{

    use RedirectsUsers;

    protected $redirectTo = '/';

    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    public function register()
    {

        $currentYear = date('Y');
        return view('reg.register')->with(compact('currentYear'));

    }

    public function postRegister(Request $request)
    {
        
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }
      
        // Create new User
		$user = new User;
        // Get all the inputs
        $input = $request->all();
        // assign values
        $user->name = $input['name'];
        $user->dayOfBirth = $input['dayOfBirth'];
		$user->monthOfBirth = $input['monthOfBirth'];
		$user->yearOfBirth = $input['yearOfBirth'];
		$user->email = $input['email'];
		$user->password = bcrypt($request['password']);
		
		if ($request->hasFile('image')) {
         
            // get the file
            $image = $request->file('image');
            // change the name of the file
            //$filename  = $user->email . '.' . $image->getClientOriginalExtension();
            $filename  = $user->email . '.' .'png';
            // save the new image to public/images
            $path = public_path('images/users/' . $filename);
            // create a new Image data
            $path = Image::make($image->getRealPath())->resize(300, 300)->save($path)->encode('data-url');
            // save the image data to its user
            $user->image = $path;
           
        }	
		
		 
		// save the user Object
	    $user->save();	
		
			
        return redirect()->route('post.index');

    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
   

}
