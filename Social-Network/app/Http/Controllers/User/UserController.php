<?php

namespace App\Http\Controllers\User;

use App\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use Input;
use DB;
use Image;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // get all the users
        $user = User::all();

        return view('users.view')->with(compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        // get the users id
        $user = User::find($id);

        // calculate the users age
        $dob = $user->yearOfBirth;
        $currentYear = date('Y');
        $dob = $currentYear - $dob;

        $image = $user->image;
        //$image = Input::file($userImage);
        //$image = $_FILES[$user->image];
       // $image = Input::file($user->image)->getRealPath();

        return view('users.show')->with(compact('user', 'dob', 'image'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        // get the user's id
        $user = User::find($id);
        $year = $user->yearOfBirth;
        $currentYear = date('Y');
        // return the view with the $id
        return view('users.edit')->with(compact('user', 'currentYear', 'year'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        // get the user's id
        $user = User::find($id);

        $validator = $this->validator($request->all());

         if($validator->passes()){

           // Get all the inputs
                $input = $request->all();
                // assign values
                $user->name = $input['name'];
                $user->dayOfBirth = $input['dayOfBirth'];
        		$user->monthOfBirth = $input['monthOfBirth'];
        		$user->yearOfBirth = $input['yearOfBirth'];
        		$user->email = $input['email'];
        		$user->password = bcrypt($request['password']);
        		
        		if ($request->hasFile('image')) {
                 
                    // get the file
                    $image = $request->file('image');
                    // change the name of the file
                    $filename  = $user->email . '.' .'png';
                    // save the new image to public/images
                    $path = public_path('images/users/' . $filename);
                    // create a new Image data
                    $path = Image::make($image->getRealPath())->resize(300, 300)->save($path)->encode('data-url');
                    // save the image data to its user
                    $user->image = $path;
                   
                }	
		
		 
    		// save the user Object
    	    $user->save();	
		
          
           return redirect()->route('user.show', [$user->id]);
        

        } else{

            return redirect()->back()->withErrors($validator);
        }
        
    }

     /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255',
          //  'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        // get the user's id
        $user = User::find($id);

        $user->delete();

        return redirect()->route('post.index');
    }

    public function search(){
    
        $input = Input::all();
        
        $i = $input['query'];
        $mySearch = $i;
        
    //  $userImage = User::all();
    
        //$searchuser = User::where('email','like',$i)->get();
        
         $sql =  "SELECT * FROM users WHERE name like ?";
        $user = DB::select($sql, array("%$i%"));
     
        return view('users.view')->with(compact('user', 'mySearch'));;
        // return View::make('users.view')->with(compact('mySearch', 'searchuser'));
    //  return View::make('users.search')->with('mySearch',$mySearch)->with('searchuser', $searchuser);
    }
}
