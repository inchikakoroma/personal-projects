<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|


//Route::get('/', 'WelcomeController@index');

// Home Page
Route::get('/', function () {
    //return view('posts.index');
 
    $img = Image::make('foo.jpg')->resize(300, 200);

    return $img->response('jpg');

});

Route::get('home', 'HomeController@index');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
*/




// Home Page
Route::get('/', function () {
    return view('posts.index');
});

// Submit the Registration Form
Route::post('register', 'Reg\RegistrationController@postRegister');

// Get the Registration Page
Route::get('register', 'Reg\RegistrationController@register');

// Get the Login Page
Route::get('login', 'Login\LoginController@login');

// Submit the Login Page
Route::post('login', 'Login\LoginController@postLogin');

// Log the User out
Route::get('logout', 'Login\LoginController@logout');

Route::post('user/search', array('as' => 'user.search', 'uses' => 'User\UserController@search'));

// Controllers
Route::controllers([
   'password' => 'Auth\PasswordController',
   'login' => 'Login\LoginController',
   'reg' => 'Reg\RegistrationController'
]);

// Post controller routes
Route::resource('post', 'Post\PostController');

// User controller routes
Route::resource('user', 'User\UserController');
