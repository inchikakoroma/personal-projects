@extends('reg.layout')

@section('title')
Database Search Results
@stop

@section('content')
  <div class="row">
    <div class="col-sm-2">
         
      
            
          
    </div>
    <div class="col-sm-10">
           <h4>Profile Info</h4>
            <ul class="list-group">
                
                <li class="list-group-item">
                     
                        <div class='post'>
                         <img class='post-image' src="{{{ $user->image }}}" alt='photo' />
                        
                       
                          <br>
                     
                            <strong style="text-align:right; margin-left:20px;">Name:</strong> {{{ $user->name }}}
                            <br>
                            <strong style="text-align:right; margin-left:20px;">Age:</strong> {{{ $dob }}}
                          <br>
                              <strong style="text-align:right; margin-left:20px;">Email:</strong> {{{ $user->email }}}
                          <br>
                        </div>
                        <p>
                            {!! Form::open(array('method' => 'DELETE', 'route' => 
                                 array('user.destroy', $user->id))) !!}
                            {!! Form::submit('Delete', array('class' => 'btn btn-danger')) !!}
                            <a class="btn btn-primary" href="{{ route('user.edit', [$user->id]) }}" role="button">Edit</a>
                                 
                           
                                 @if(Auth::id() == $user->id)
                                
                                @else
                                 <a class="btn btn-success" href="#" role="button">Add Friend</a>
                                @endif
                            
                            {!! Form::close() !!}
                        </p>
                       
                 </li>
                
                 
                
            </ul>
          
    </div>
  </div>
 @stop
