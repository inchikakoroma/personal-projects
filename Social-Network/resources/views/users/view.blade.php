@extends('users.layout')

@section('title')
Database Search Results
@stop

@section('content')


      <div class="row">
        

        <div class="col-sm-2">
            @if (Auth::check())
                              
                <p>
                  <strong>Welcome:</strong> {!! Auth::user()->name !!} 
                </p> 
               
            @endif
        </div>


       
        <div class="col-sm-10">
           
            <br>
            <br>
             
            @if (count($user) == 0)
             
              <p>No USER found.</p>
            
            @else 
              
            <h5> {{ Count($user) }} users found</h5>
               
              <ul class="list-group">
                  
                  <li class="list-group-item">
                  
                    @foreach($user as $users)
                      
                      <div class='post'>
                          
                           <img class='post-image' src="{{{ $users->image }}}" alt='photo' />
                          <br>
                          <strong style="text-align:right; margin-left:20px;">Name:</strong> {{{ $users->name }}}
                          <br>
                           
                          <br>
                          
                          <br>
                             
                      </div>
                        
                      <p>
                        <a class="btn btn-info" href="{{ route('user.show', [$users->id]) }}" role="button">Profile</a>
                      <p>
                    
                    @endforeach
                  
                  </li>
    
              </ul>
      
                    
            @endif
          
        </div>


        
      </div>
      
@stop

