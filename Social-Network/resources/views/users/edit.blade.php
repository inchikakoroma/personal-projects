@extends('reg.layout')

@section('title')
Update User
@stop

 @section('content')
    
    <div class="col-sm-1">
           
    </div>


        <div class="col-sm-9">
                
          <h3>Edit Details</h3>
                <br>
                <br>
                {!! Form::model($user, array('method' => 'PUT', 'files' => true, 
                    'route' => array('user.update', $user->id))) !!}
              
              <div class="form-group">
                 {!! Form::label('name', 'Name: ') !!}
                 {!! Form::text('name', $user->name, array('class' => 'form-control')) !!}
                 {!! $errors->first('name') !!}
              </div>

              <div class="form-group">
                {!! Form::label('date_of_birth', 'Date Of Birth: ') !!} <br>
                {!! Form::selectRange('dayOfBirth', $user->dayOfBirth, 01, 31) !!}
                {!! Form::selectMonth('monthOfBirth', $user->monthOfBirth) !!}

                {!! Form::selectYear('yearOfBirth', $year, $currentYear, $currentYear) !!}       
              </div>

              <div class="form-group">
                 {!! Form::label('email', 'Email: ') !!}
                 {!! Form::email('email', $user->email, array('class' => 'form-control')) !!}
                 {!! $errors->first('email') !!}
              </div>
              
              <div class="form-group">
                {!! Form::label('image', 'Profile Image: ') !!}
                {!! Form::file('image') !!}
              </div>
              
              
              {!! Form::submit('Save', array('class' => 'btn btn-success')) !!}
              {!! Form::close() !!}
             
         
        </div>
        
    <div class="col-sm-1">
           
    </div>

 @stop