    
@extends('login.layout')

@section('title')
Login 
@stop

 @section('content')
    
    <div class="col-sm-1">

       
           
    </div>


        <div class="col-sm-10">
                
          <div style="padding-top:30px" class="panel-body" >

                        @if (count($errors) > 0)
                          <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                              <ul>
                                @foreach ($errors->all() as $error)
                                  <li>{{ $error }}</li>
                                @endforeach
                              </ul>
                          </div>
                        @endif 
                        
                            <div style="margin-bottom: 25px" class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                {!! Form::open(array('url' => ('login'))) !!}
                                <input type="hidden" class="form-control" name="email" value="{!! csrf_token() !!}">
                                    
                                <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                                                                   
                            </div>
                                
                            <div style="margin-bottom: 25px" class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                   
                                    {!! Form::password('password', array('class' => 'form-control')) !!}
                                   
                            </div>

                            <div class="checkbox">
                              <label>
                                <input type="checkbox" name="remember"> Remember Me
                              </label>
                            </div>

                            <div style="margin-top:10px" class="form-group">
                            <!-- Button -->

                              {!! Form::submit('Sign in', array('class' => 'btn btn-success')) !!}
                              {!! Form::close() !!}
                                    
                            </div>
                            
                                <div class="form-group">
                                    
                                  <br>
                                  Don't have an account!
                                  <a href="{{{ url('register') }}}" role="button"> Sign Up Here</a>
                                </div>
                                
            </div> 
             
         
        </div>
        
    <div class="col-sm-1">
           
    </div>
 
 @stop