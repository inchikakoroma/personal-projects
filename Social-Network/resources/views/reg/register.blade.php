@extends('reg.layout')

@section('title')
Create User
@stop

 @section('content')
    
    <div class="col-sm-1">
           
    </div>


        <div class="col-sm-10">
                
          <h3>Create New User</h3>
                <br>
                <br>
                {!! Form::open(array('url' => ('register'), 'files' => true)) !!}

              <div class="form-group">
                 {!! Form::label('name', 'Name: ') !!}
                 <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                 {!! $errors->first('name') !!}
              </div>

              <div class="form-group">
                {!! Form::label('date_of_birth', 'Date Of Birth: ') !!} <br>
                {!! Form::selectRange('dayOfBirth', 01, 31) !!}
                {!! Form::selectMonth('monthOfBirth') !!}
                {!! Form::selectYear('yearOfBirth', 1930, $currentYear) !!}       
              </div>

              <div class="form-group">
                 {!! Form::label('email', 'Email: ') !!}
                 <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                 {!! $errors->first('email') !!}
              </div>

              <div class="form-group">
                {!! Form::label('password', 'Password: ') !!}
                {!! Form::password('password', array('class' => 'form-control')) !!}
                {!! $errors->first('password') !!}
              </div>

              <div class="form-group">
                {!! Form::label('password', 'Confirm Password: ') !!}
                {!! Form::password('password_confirmation', array('class' => 'form-control')) !!}
                {!! $errors->first('password') !!}
              </div>

              <div class="form-group">
                {!! Form::label('image', 'Profile Image: ') !!}
                {!! Form::file('image') !!}
                
              </div>
              
              
              {!! Form::submit('Create', array('class' => 'btn btn-success')) !!}
              {!! Form::close() !!}
             
         
        </div>
        
    <div class="col-sm-1">
           
    </div>
 
 @stop