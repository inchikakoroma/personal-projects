<?php

include("../connection.php");
include("../header.php");

error_reporting(E_ALL);
ini_set('display_errors', 1);


if(isset($_POST['mySubmit'])) {
    // add a new user
    $add = addUser($mysqli);

    // user alert message

   ?>

<script type="text/javascript">

   $(document).ready(function(){

	   // trigger modal
	   $("#myModal").modal();

	   // when the close button is triggered redirect user
	   $(".close, .btn-default").on("click", function(){
	   		window.location.href = "../index.php";
	   });

   });
   

</script>

    <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">×</button>
          <h4 class="modal-title">Infomation</h4>
        </div>
        <div class="modal-body">
          <p> <?php print $add; ?> </p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>


   <?php

    
}


// register new user function
function addUser($conn){


	$firstName = $_POST["firstName"];
	$lastName = $_POST["lastName"];
	$email = $_POST["email"];
	$password = password_hash($_POST["password"], PASSWORD_DEFAULT);
	$ticketNumber = $_POST["ticketNumber"]; 
	$phoneNumber = $_POST["phoneNumber"];
	$dateOfBirth = $_POST["dateOfBirth"];

	// set default date
	date_default_timezone_set("Australia/Brisbane");
	$timeStamp =  date("Y-m-d H:i:s");

	// change dob format
	$date2 = DateTime::createFromFormat('d-m-Y', $dateOfBirth)->format('Y-m-d');

	$sql = "INSERT INTO user (firstname, lastname, email, password, ticketNumber, phoneNumber, dateOfBirth, created_at)
			VALUES ('$firstName', '$lastName', '$email', '$password', '$ticketNumber', '$phoneNumber', '$date2', '$timeStamp')";

	if (!mysqli_query($conn, $sql)){
  		die('Insert Error: ' . mysql_error());
  	}

  	//echo "recored added";
  	$conn->close();

  	$message = "User Added";
  	
  	return $message;

}

?>