<?php

//require_once “PeopleFamily.php";

require_once '../PeopleFamily.php';

class PeopleFamiliesTest extends PHPUnit_Framework_TestCase{

    /*
    * create a new PeopleFamily Class

    */

    public function setup(){

        $this->families = new PeopleFamily();
    }
    

    /**
    * Test for valid Family ID
    *
    * @param  string $personID Person's ID
    * @param  string $familyID Family ID of the person
    * @return This test should pass if the person's family ID is equals to the given family ID.
    */

    public function testFamilyID(){

        $personID = "3";
        $familyID = "1";

        $this->assertEquals($familyID, $this->families->getFamilyID($personID));
    }


    /**
    * Test Merging two families together.
    *
    * @param  string $primaryFamilyID Primary family ID after the merge
    * @param  string $secondFamilyID Family ID to merge with the primary family
    * @return This test should fail if either person's family ID's is invalid.
    */

    public function testMergeFamilies(){

        $primaryFamilyID = "5";
        $secondaryFamilyID = "6";

        $this->assertEquals(true, $this->families->mergeFamilies($primaryFamilyID, $secondaryFamilyID));

    }

    /**
    * Test if the person has a big family
    *
    * @param  string $personID
    * @return This test should pass if the person has 5 or more family members.
    */

    public function testBigFamily(){

        $personID = "10";

        $this->assertEquals(true, $this->families->hasBigFamily($personID));

    }  

}