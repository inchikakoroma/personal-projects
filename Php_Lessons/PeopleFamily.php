<?php

	error_reporting(E_ALL);
	ini_set('display_errors', 1);

	include('DB.php');

	$db = new Db(); 

	
	class PeopleFamily{


		/**
		* Returns a person's family ID
		*
		* @param  string $person_id
		* @return string $familyID
		*/

		public function getFamilyID($person_id){

			// init database connection
			global $db;

			// select query
			$person = $db->selectFirst("SELECT family_id FROM person WHERE id = '" . $person_id . "' ");

			if(!$person){
			
				return false;
			
			}else{

				// if the person exists get his family ID
				$familyID = $person['family_id'];

				return $familyID;

			}


		}

		/**
		* Returns all members of the provided family.
		* @param  string $family_id
		* @return array[]
		*/

		public function checkFamily($family_id){

			// init database connection
			global $db;

			// select query
			$familyMembers = $db->select("SELECT id FROM person WHERE family_id = '" . $family_id . "' ");

			if(!$familyMembers){

				// if no family member is found
				return false;

			}

			return $familyMembers;

		}

		/**
	    * Returns true if a person belongs to a 'big' family.
	    *
	    * @param  string $person_id
	    * @return bool
	    */

	    public function hasBigFamily($person_id){
        
        	// get the person's family members
        	$family = self::findFamilyMembers($person_id);

        	// count family members
        	$count = count($family);

        	// if family members equals or greater than 5
        	if($count >= 5){

        		return true;

        	}else{

        		return false;
        	}

        	
    	}

		/**
		* Returns all members of the provided person's family.
		*
		* @param  string $person_id
		* @return array[]
		*/

		public function findFamilyMembers($person_id){

			// init database connection
			global $db;

			// get the person's family ID
			$familyID = self::getFamilyID($person_id);

			// select query
			$familyMembers = $db->select("SELECT id, name, family_id FROM person WHERE family_id = '" . $familyID . "' ");

			if(!$familyMembers){
				
				// if no family member is found
				return false;
			}

			return $familyMembers;

		}

    	/**
     	* Merge two families together.
     	*
     	* @param  string $primaryFamily Primary family ID after the merge
     	* @param  string $secondaryFamily Family ID to merge with the primary family
     	* @return bool False if either of the families doesn't exist
     	*/

    	public function mergeFamilies($primaryFamily, $secondaryFamily){

    		// init database connection
			global $db;

    		// get and check if the family exists
			$firstFamily = self::checkFamily($primaryFamily);
        	$secondFamily = self::checkFamily($secondaryFamily);

        	if($firstFamily && $secondFamily){
        		
        		// Merge the two families
        		$update = $db->query("UPDATE person SET family_id = '" . $primaryFamily. "' WHERE family_id = '" . $secondaryFamily. "' ");

        		if($update === true){

        			return true;

        		}

        	}else if($firstFamily){

        		// if second family is invalid
				return false;

        	}else if(!$firstFamily && !$secondFamily){

        		// if both families are invalid
				return false;

        	}else{
        		
        		// if first family is invalid
        		return false;

        	}

    	}


	}



	