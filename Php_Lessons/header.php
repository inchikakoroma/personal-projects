<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);


define("LOGIN", "logged", true);


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>PHP Lessons</title>
   
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/jquery-ui-1.12.0.css">

    <!-- Styles -->
    <link rel="stylesheet" href="" >
    

    <style>
        body {
            font-family: 'Lato';
        }

        .fa-btn {
            margin-right: 6px;
        }
    </style>
</head>
<body id="app-layout">
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="#">
                    PHP Lessons
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    <li><a href="http://localhost:8888/personal-projects/Php_Lessons">Home</a></li>
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    
                        <li><a href="http://localhost:8888/personal-projects/Php_Lessons/lesson1/login.php">Login</a></li>
                        <li><a href="#">Help</a></li>
                        <?php 

                        if(LOGIN != "logged"){

                        ?>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                 <span class="caret"></span>
                            </a>
                            
                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <a href=""><i class="fa fa-btn fa-sign-out"></i>Logout</a>
                                </li>
                            </ul>    
      
                        </li>

                        <?php 
                                }else{

                                }
                            ?> 
                    
                </ul>
            </div>
        </div>
    </nav>

    <script src="/js/jquery-2.2.4.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/jquery-ui-1.12.0.js"></script>
    <!-- CryptoJS -->
    <script src="http://localhost:8888/personal-projects/Php_Lessons/js/sha1.js"></script>
    <script src="http://localhost:8888/personal-projects/Php_Lessons/js/scripts.js"></script>
    
</body>
</html>
