<?php


	function convert($amount){


		$decimalPoint = " AND ";
    	$cents     = " Cents";
    	$word = null;
    	$decimalPart = null;
        $dollars   =  " Dollars";

    	// numbers into words array
    	$numWords  = array(
        0 => 'Zero',
        1 => 'One',
        2 => 'Two',
        3 => 'Three',                   
        4 => 'Four',                   
        5 => 'Five',                  
        6 => 'Six',                  
        7 => 'Seven',                  
        8 => 'Eight',                  
        9 => 'Nine',                  
        10 => 'Ten',                  
        11 => 'Eleven',                  
        12 => 'Twelve',                  
        13 => 'Thirteen',                  
        14 => 'Fourteen',               
        15 => 'Fifteen',                 
        16 => 'Sixteen',                 
        17 => 'Seventeen',                 
        18 => 'Eighteen',                 
        19 => 'Nineteen',                
        20 => 'Twenty',                 
        30 => 'Thirty',                 
        40 => 'Fourty',                
        50 => 'Fifty',                
        60 => 'Sixty',                 
        70 => 'Seventy',                
        80 => 'Eighty',                 
        90 => 'Ninety',                 
        100 => 'Hundred',                
        1000 => 'Thousand',
        10000 => 'Ten Thousand',  
        100000 => 'Hundred Thousand',             
        1000000 => 'Million',           
        1000000000 => 'Billion',         
        1000000000000 => 'Trillion',     
        1000000000000000 => 'Quadrillion',   
        1000000000000000000 => 'Quintillion');


		// double check if input is a valid number
		if (!is_numeric($amount)) {
			$errorMsg = "Not A Valid Number";
	        return $errorMsg;
	    }

        if ($amount > 9999999) {
            $errorMsg = "Number must not be greater than 10 Million";
            return $errorMsg;
        }

	    // split amount if there is a decimal point
	    if (strpos($amount, '.') !== false) {
	        list($amount, $decimalPart) = explode('.', $amount);
	    }

        if($decimalPart > 99){
            $errorMsg = "Please Round Up Your Figures";
            return $errorMsg;
        }

	    if($amount < 21){
	    	
	    	// convert words to string
            if($amount == 1){
                $word = $numWords[$amount];
            }else{
                $word = $numWords[$amount];
            }
            

	    }else if($amount < 100){
	    	// split amount into two places (tens and ones)
            // tens
	    	$num1 = ((int)($amount / 10) * 10);
            // ones
	    	$num2  = $amount % 10;
	    	
	    	// convert number into words
	    	$word = $numWords[$num1];

	    	// check if there is a ones column
	    	if($num2){
	    		$word .= " " . $numWords[$num2];
	    		//$string .= $hyphen . $dictionary[$units];
	    	}
	    	
	    }else if($amount < 1000){
            // split amount into three places (tens and ones and hundreds)
            $num1 = ((int) ($amount / 100));
            $num2 = ((int)($amount % 100) / 10);
            $num2 = floor($num2);
            $num3  = $amount % 10;

          //  print_r($num1 . " num1 " . $num2 . " num2 " . $num3 . " num3 <br>");

                $word = $numWords[$num1] . " " . $numWords[100];

                if($num2 > 1){
                    // numbers greater than 1 
                $num2 = $num2 * 10;
                $word .= " And " . $numWords[$num2];
                $word .= " " . $numWords[$num3];

                }else if($num2 == 1){

                    $num2 = ((int)($amount - 100));
                    $word .= " And " . $numWords[$num2];

                }else if($num2 == 0 && $num3 >= 1){
                    
                    $word = $numWords[$num1] . " " . $numWords[100] . " And " . $numWords[$num3];
                    
                }else if($num2 == 0 & $num3 == 0){
                    
                    $word = $numWords[$num1] . " " . $numWords[100];

                }

        }else{
           
            $splitNum = str_split((int) $amount);
            
            // convert thousand
            if(sizeof($splitNum) == 4){
                
                $num = $splitNum[0];
                $num1 = $splitNum[2];
                $num3 = $splitNum[3];

                $word = $numWords[$num] . " " . $numWords[1000];

                if($splitNum[1] > 0){
                    $num = ($splitNum[1]);
                    $word .= " " . $numWords[$num] . " " . $numWords[100];
                }

                if($splitNum[2] > 0){
                    
                    if($num1 > 1){
                        
                        $num1 = $num1 * 10;
                        $word .= " And " . $numWords[$num1];
                        $word .= " " . $numWords[$num3];

                    }else if($num1 == 1){
                        
                        $num1 = $num1 * 10;
                        // join the next digit 
                        
                        $num1 = $num1 + $num3;
                        $word .= " And " . $numWords[$num1];

                    }

                }else{

                    if($splitNum[3] > 0){
                        $word .= " And " . $numWords[$num3];
                    }
                    
                }

                
            }

             // convert ten thousand
            if(sizeof($splitNum) == 5){


                $tenThousand = $splitNum[0];
                $thousand  = $splitNum[1];
                $hundred = $splitNum[2];
                $tens = $splitNum[3];
                $ones = $splitNum[4];

                if($tenThousand == 9 && $thousand == 0){
                    $th2 = $tenThousand * 10;
                    $word .= $numWords[$th2] . " " . $numWords[1000];
                }else if($tenThousand > 1){
                    $th2 = $tenThousand * 10;
                    $word .= $numWords[$th2] . " " . $numWords[$thousand]. " " . $numWords[1000];
                }else if($thousand == 0){
                   $th2 = $tenThousand * 10;
                    $word .= $numWords[$th2] . " " . $numWords[1000];
                }else if($thousand > 0){
                    $th2 = $tenThousand * 10 + $thousand;
                    $word .= $numWords[$th2] . " " . $numWords[1000];
                }


                if($hundred > 0){
                    $num = ($hundred);
                    $word .= " " . $numWords[$num] . " " . $numWords[100];
                }

                if($tens > 0){
                    
                    if($tens > 1){
                        
                        $tens = $tens * 10;
                        $word .= " And " . $numWords[$tens];
                        $word .= " " . $numWords[$ones];

                    }else if($tens == 1){
                        
                        $tens = $tens * 10;
                        // join the next digit 
                        
                        $tens = $tens + $ones;
                        $word .= " And " . $numWords[$tens];

                    }

                }else{

                    if($ones > 0){
                        $word .= " And " . $numWords[$ones];
                    }
                    
                }
                   
            }


            // convert hundred thousand
            if(sizeof($splitNum) == 6){

                $hundredThousand  = $splitNum[0];
                $tenThousand = $splitNum[1];
                $thousand  = $splitNum[2];
                $hundred = $splitNum[3];
                $tens = $splitNum[4];
                $ones = $splitNum[5];


                $word = $numWords[$hundredThousand] . " " . $numWords[100];

                if($tenThousand == 9 && $thousand == 0){
                    
                    $th2 = $tenThousand * 10;
                    $word .= " And " . $numWords[$th2] . " " . $numWords[1000];

                }else if($tenThousand > 1){

                    $th2 = $tenThousand * 10;
                    $word .= " And " . $numWords[$th2] . " " . $numWords[$thousand] . " " . " " . $numWords[1000];

                }else if($tenThousand == 1 && $thousand >= 0){

                    $th2 = $tenThousand * 10 + $thousand;
                    $word .= " And " . $numWords[$th2] . " "  . $numWords[1000];

                }else if($thousand == 0){

                   $th2 = $tenThousand * 10;
                    $word .= " " . $numWords[1000];

                }else if($thousand > 0){

                    $th2 = $tenThousand * 10 + $thousand;
                    $word .= " And " . $numWords[$th2] . " " . $numWords[1000];

                }


                if($hundred > 0){

                    $num = ($hundred);
                    $word .= " " . $numWords[$num] . " " . $numWords[100];

                }

                if($tens > 0){
                    
                    if($tens > 1){
                        
                        $tens = $tens * 10;
                        $word .= " And " . $numWords[$tens];
                        $word .= " " . $numWords[$ones];

                    }else if($tens == 1){
                        
                        $tens = $tens * 10;
                        // join the next digit 
                        
                        $tens = $tens + $ones;
                        $word .= " And " . $numWords[$tens];

                    }

                }else{

                    if($ones > 0){
                        $word .= " And " . $numWords[$ones];
                    }
                    
                }
            
            }

            // convert one million
            if(sizeof($splitNum) == 7){

                $million = $splitNum[0];
                $hundredThousand  = $splitNum[1];
                $tenThousand = $splitNum[2];
                $thousand  = $splitNum[3];
                $hundred = $splitNum[4];
                $tens = $splitNum[5];
                $ones = $splitNum[6];

              
                   
                $word .= $numWords[$million] . " " . $numWords[1000000];

                if($tenThousand == 9 && $thousand == 0){

                    $th2 = $tenThousand * 10;
                    $word .= " And " . $numWords[$th2] . " " . $numWords[1000];

                }else if($tenThousand > 1){
                    $th2 = $tenThousand * 10;
                    $word .= " " .$numWords[$hundredThousand] . " " . $numWords[100] . " And " . $numWords[$th2] . " " . $numWords[$thousand] . " " . " " . $numWords[1000];
                }else if($tenThousand == 1 && $thousand >= 0){
                    $th2 = $tenThousand * 10 + $thousand;
                    $word .= " " . $numWords[$hundredThousand] . " " . $numWords[100] . " And " . $numWords[$th2] . " "  . $numWords[1000];
                }else if($thousand == 0){
                    $th2 = $tenThousand * 10;
                    if($hundredThousand == 0 && $tenThousand == 0 && $hundred == 1){
                        $word .= " And ";
                    }else if($hundred == 0 && $tens == 0 && $hundred == 0 && $hundredThousand == 0){
                    }else{
                        $word .= " " . $numWords[$hundredThousand] . " " . $numWords[100] . " " . $numWords[1000];
                    }
                }else if($thousand > 0){
                    $th2 = $tenThousand * 10 + $thousand;
                    $word .= " " .$numWords[$hundredThousand] . " " . $numWords[100] . " And " . $numWords[$th2] . " " . $numWords[1000];
                }


                if($hundred > 0){
                    $num = ($hundred);
                    $word .= " " . $numWords[$num] . " " . $numWords[100];
                }

                if($tens > 0){
                    
                    if($tens > 1){
                        
                        $tens = $tens * 10;
                        $word .= " And " . $numWords[$tens];
                        $word .= " " . $numWords[$ones];

                    }else if($tens == 1){
                        
                        $tens = $tens * 10;
                        // join the next digit 
                        
                        $tens = $tens + $ones;
                        $word .= " And " . $numWords[$tens];

                    }

                }else{

                    if($ones > 0){
                        $word .= " And " . $numWords[$ones];
                    }
                    
                }

                
               
            }

        }

        $wordsWithDollar = null;

        if($word == "One"){

            // Add dollar to the end of the word
            $splitWords = explode(" ", $word);
            array_push($splitWords, " Dollar ");

            $wordsWithDollar = implode(" ", $splitWords);

        }else{

            // Add dollar to the end of the word
            $splitWords = explode(" ", $word);
            array_push($splitWords, " Dollars ");

            $wordsWithDollar = implode(" ", $splitWords);

        }

       

	    // Decimal Part
	    if ($decimalPart){

	    	$num1 = ((int)($decimalPart / 10) * 10);
	    	
            $num2  = $decimalPart % 10;

            //print_r($num1 . " num1 " . $num2 . " num2");

            //echo "<br>";

            if($num1 == 0 && $num2 == 1){
                $decimalWord = $numWords[$num2] . " Cent";
            }else if($num1 == 0){
                $decimalWord = $numWords[$num2] . $cents;
            }else if($num1 == 10){
                $num1 = $num1 + $num2;
                $decimalWord = $numWords[$num1] . $cents;
            }else if ($num1 > 1 && $num2 == 0){
                $decimalWord = $numWords[$num1] . $cents;
            }else if($num1 > 1 && $num2 > 0){
                $decimalWord = $numWords[$num1] . " " . $numWords[$num2] . $cents;
            }
          
        	
        	$wordsWithDollar .= $decimalPoint . $decimalWord;
    	}

        
		return $wordsWithDollar;
	}

?> 