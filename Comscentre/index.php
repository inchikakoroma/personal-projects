<?php

    include("convert.php");

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Numbers To Words</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

    <style>
        
        body {
            font-family: 'Lato';
            
        }

        .footer {
          position: absolute;
          bottom: 0;
         
          height: 40px;
        }

    </style>

</head>

<body>

        
<!-- Top Header -->

<nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="#">
                    Comscentre
                </a>
            </div>

            
        </div>
    </nav>

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Convert Numbers To Words</div>
                <div class="panel-body">
                <form role="form" id="form" method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
                    
                    <div class="form-group">
                        <label for"ticketNumber">Amount: (Between 1 - 9 Million)</label>
                        <input type="text" name="amount"  class="form-control" id="amount"></input>
                        <label id="amountError"  style="display: none;">You Must Enter A Valid Number</label>

                    </div>

                    <div class="form-group">
                        <input type="submit" id="convertButton" name="convertButton" class="btn btn-success" value="Convert">
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>


<!--Footer -->
<footer class="footer">
  <div class="container">
    <p class="text-muted">By: Inchika Alan Koroma</p>
  </div>
</footer>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>

        <script type="text/javascript">

    $(document).ready(function(){

        if (typeof jQuery !== 'undefined') {
            
            (function($) {
                $('#spinner').ajaxStart(function() {
                    $(this).fadeIn();
                }).ajaxStop(function() {
                    $(this).fadeOut();
                });
            })(jQuery);
        }

        
        // Amount Error
        $("#amount").on("blur", function(){

           var amount = $("#amount").val().replace(/\s/g, "");

           if(!$.isNumeric(amount) || amount.length == "" || this.value < 0) {
                // show errors 
                showErrorText("#amountError");
                showErrorColor("#amount");
                
            }else{
                // hide errors
                hideErrorText("#amountError");
                hideErrorColor("#amount");

            }

            this.value = amount;

        });

        //Error Checking before form is Submitted
        $("#form").submit(function(e){
           
           var $input = $(this).find("input[name=amount]");

           if ($input.val() <= 0){
                e.preventDefault();
                showErrorText("#amountError");
                showErrorColor("#amount");
           }else{
                hideErrorText("#amountError");
                hideErrorColor("#amount");
           }

          
        });

    
        

    });

    // Error Functions

    function showErrorText(id){
        // show error text
        $(id).css("display","inline");
        $("#convertButton").prop('disabled', true);
    }

    function showErrorColor(id){
        // color border color
        $(id).css("border-color", "red");
    }

    function hideErrorText(id){
        // hide errors
        $(id).css("display","none");
        $("#convertButton").prop('disabled', false);
    }

    function hideErrorColor(id){
        // hide errors
        $(id).css("border-color", "#66afe9");
    }


</script>
</body>
    
</html>

<?php

if(isset($_POST['convertButton'])){
    
    // display results
    $myVal = convert($_POST["amount"]);

    echo "<script type='text/javascript'>
            var $ = jQuery.noConflict(); 
            $(document).ready(function(){
                $('#myModal').modal('show');
            });
        </script>";
}

?>

<!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">×</button>
          <h4 class="modal-title">Results</h4>
        </div>
        <div class="modal-body">
          <p> <?php print $myVal; ?> </p>
        </div>
        <div class="modal-footer">
          <buttontype="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>

