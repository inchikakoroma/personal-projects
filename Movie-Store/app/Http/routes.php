<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
|--------------------------------------------------------------------------
| Customers Front-end Routes
|--------------------------------------------------------------------------
|
*/

// Home Page

Route::get('/', function () {
    return view('auth.login');
});


/*
|--------------------------------------------------------------------------
| Authentication & Registration Routes
|--------------------------------------------------------------------------
|
*/

// get login page
Route::get('auth/login', [
    'middleware' => 'auth',
    'as' => 'login', 
    'uses' => 'Auth\AuthController@getLogin'
]);


//Route::get('auth/login', 'Auth\AuthController@getLogin');

// submit login
Route::post('auth/login', 'Auth\AuthController@postLogin');

// logout
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Reset Password
Route::controllers([
   'password' => 'Auth\PasswordController',
]);


/*
|--------------------------------------------------------------------------
| User Routes
|--------------------------------------------------------------------------
|
*/


// show user's profile page
Route::get('staff/profile/{id}', 
    [
        'middleware' => 'auth',
        'as' => 'profile', 
        'uses' => 'UserController@showProfile', 
        function($id) {
    //
        }
    ]
);

// edit a user by ID

Route::get('staff/edit/{id}', 
    [
        'middleware' => 'auth',
        'as' => 'editUser', 
        'uses' => 'UserController@edit', 
        function($id) {
    //
        }
    ]
);

// show all staff

Route::get('staff', [
    'middleware' => 'auth',
    'as' => 'showStaff', 
    'uses' => 'UserController@showUsers'
]);

// show all users (ADMIN)

Route::get('admin-users', [
    'middleware' => 'auth',
    'as' => 'adminView', 
    'uses' => 'UserController@show'
]);

// update a user

Route::post('staff/update/{id}', 
    [
        'middleware' => 'auth',
        'as' => 'updateStaff', 
        'uses' => 'UserController@update', 
        function($id) {
    //
        }
    ]
);

// add new user

Route::post('user/new', 
    [
        'middleware' => 'auth',
        'as' => 'addStaff', 
        'uses' => 'UserController@addUser', 
        function($id) {
    //
        }
    ]
);

// delete a user
Route::get('staff/delete/{id}', 
    [
        'middleware' => 'auth',
        'as' => 'deleteStaff', 
        'uses' => 'UserController@destroy', 
        function($id) {
    //
        }
    ]
);

/*
|--------------------------------------------------------------------------
| Staff Back-end Routes
|--------------------------------------------------------------------------
|

*/

// get the Staff Controller index page
Route::get('dashboard', [
    'middleware' => 'auth',
    'uses' => 'StaffController@index'
]);

// get the Create New Staff Page
Route::get('staff/new', [function() {
    
        // get the login user
        $user =  Auth::user();
        
        // change this if you want to add an admin (new applications)
        
        if($user->type == "Admin"){
            
            // return the create new staff page
            return view('auth.register');
            
        }else{
            return redirect()->back();
        }
    
}]);

// listen to user login events
Event::listen('auth.logout', function($user) {
    
    
    $dt = new DateTime();
    
    $dt->format('D-M-Y H:i:s');
   
    $user->active = $dt;

    $user->save();
});


/*
|--------------------------------------------------------------------------
| Movie Routes
|--------------------------------------------------------------------------
|

*/

// Show movies
Route::get('movies', [
   // 'middleware' => 'auth',
    'as' => 'movies', 
    'uses' => 'MovieController@index'
]);
