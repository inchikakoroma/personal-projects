<?php

namespace App\Http\Controllers;
use DB;
use Auth;
use Validator;
use Illuminate\Http\Request;
use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // get the login user
        $user =  Auth::user();

        return view('user.profile')->with(compact('user'));
    }

    /**
     * Show all staff
     *
     * @return \Illuminate\Http\Response
     */
    public function showUsers()
    {
        
        $users = User::where('type', '!=', 'Admin')->get();
        //$users = User::all()->except(Auth::id());
        
        return view('user.users', ['users' => $users]);
    }
    
    // show all Users
    
    public function show()
    {
        
        $users = User::all()->except(Auth::id());
        
        return view('user.users', ['users' => $users]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified User.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showProfile($id)
    {
        return view('user.profile', ['user' => User::findOrFail($id)]);
    }
    
  

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        return view('user.edit', ['user' => User::findOrFail($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     
     protected function validator(array $data)
   {

       return Validator::make($data, [
            'firstName' => 'required|max:255',
            'lastName' => 'required|max:255',
            'email' => 'required|email|max:255',
            'password' => 'required|confirmed|min:6',
            'type' => 'required',
        ]);
   }
     
     
    public function update(Request $request, $id)
    {
        // get the user's id
        $user = User::findOrFail($id);
        
        // validate request
        $validator = $this->validator($request->all());
        
        if($validator->passes()){

                // get all the inputs
                $input = $request->all();
                // assign values
                $user->firstName = $input['firstName'];
                $user->lastName = $input['lastName'];
                $user->gender = $input['gender'];
                $user->address = $input['address'];
                $user->email = $input['email'];
                $user->date_of_birth = $input['date_of_birth'];
                $user->storeID = $input['storeID'];
        		$user->password = bcrypt($request['password']);
        		$user->type = $input['type'];
        		
    
        
                $user->save();	
		
          
                return redirect()->route('profile', [$user->id]);
        

        }else{

            return redirect()->back()->withErrors($validator);
    
        
        }
        
    }
    
    
    // add a new user
    public function addUser(Request $request){
        
        // new user
        $user = new User;
        
        $validator = $this->validator($request->all());
        
        if($validator->passes()){

            // get all the inputs
                $input = $request->all();
                // assign values
                $user->firstName = $input['firstName'];
                $user->lastName = $input['lastName'];
                $user->gender = $input['gender'];
                $user->address = $input['address'];
                $user->email = $input['email'];
                $user->date_of_birth = $input['date_of_birth'];
                $user->storeID = $input['storeID'];
        		$user->password = bcrypt($request['password']);
        		$user->type = $input['type'];
       
           $user->save();
                
        return redirect()->route('showStaff');

        }else{

            return redirect()->back()->withErrors($validator);
    
        
        }
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // get the user's id
        $user = User::find($id);

        $user->delete();

        return redirect()->route('showStaff');
    }
    
    
}
