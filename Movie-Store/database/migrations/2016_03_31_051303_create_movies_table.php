<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMoviesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('movie', function (Blueprint $table) {
            $table->increments('film_id');
            $table->string('title');
            $table->text('description');
            $table->tinyInteger('language_id');
            $table->tinyInteger('original_language_id');
            $table->tinyInteger('rental_duration');
            $table->float('rental_rate');
            $table->tinyInteger('length');
            $table->float('replacement_cost');
            $table->enum('rating', ['G','PG','PG-13','R','NC-17'])->default('G');
            $table->enum('special_features', ['Trailers','Commentaries','Deleted Scenes','Behind the Scenes']);
            $table->timestamps();
        });
        
        DB::statement('ALTER TABLE movie ADD release_year YEAR(4) AFTER description;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //delete table
        Schema::drop('movie');
    }
}
