@extends('staff.layout')

@section('title')
Dashboard
@stop

@section('content')
 
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <small>Dashboard</small>
            </h1>
            <ol class="breadcrumb">
                <li class="active">
                    <i class="fa fa-dashboard"></i> Dashboard
                </li>
            </ol>
        </div>
    </div>
    <!-- /.row -->
    
    

</div>
<!-- /.container-fluid -->
    
    
        @if (Auth::check())
        <strong>Type:</strong> {!! Auth::user()->type !!}
        @else
        <div>
        <h5>
            Please <a href="{{{ url("./") }}}">login</a> to continue
        </h5>
    </div>
         @endif  
    
 
 @stop
 
