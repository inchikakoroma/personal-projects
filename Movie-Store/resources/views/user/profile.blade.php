@extends('staff.layout')

@section('title')
{!! $user->firstName !!} {!! $user->lastName !!}
@stop

@section('content')
 
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <small>User Profile</small>
            </h1>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-dashboard"></i>  <a href="{{{ url("dashboard") }}}">Dashboard</a>
                </li>
                <li class="active">
                    <i class="fa fa-edit"></i> User Profile
                </li>
            </ol>
        </div>
    </div>
    <!-- /.row -->
    
    

</div>
<!-- /.container-fluid -->
    
    
        @if (Auth::check())
       
      <div class="row">
        
        <div class="col-sm-1">
        </div>
      
        <div class="col-sm-10">
          
          <div class="panel panel-info">
                  <!-- Default panel contents -->
                  <div class="panel-heading">{!! $user->firstName !!} {!! $user->lastName !!}</div>
  
            <div class="panel-body">
              <div class="row">                
                <div class=" col-md-12 col-lg-12 "> 
                  <table class="table table-user-information">
                    <tbody>
                      <tr>
                        <td>Store:</td>
                        <td>{!! $user->storeID !!}</td>
                      </tr>
                      <tr>
                        <td>Hire date:</td>
                        <td>{!! $user->created_at->format('d M Y') !!}</td>
                      </tr>
                      <tr>
                        <td>Type:</td>
                        <td>{!! $user->type !!}</td>
                      </tr>
                      <tr>
                        <td>Date of Birth</td>
                        <td>{!! $user->date_of_birth->format('d-m-Y') !!}</td>
                      </tr>
                      <tr>
                        <td>Last Active</td>
                        <td>{!! $user->active->format('d M h:i') !!}</td>
                      </tr>
                      <tr>
                        <td>Gender</td>
                        <td>{!! $user->gender !!}</td>
                      </tr>
                        <tr>
                        <td>Home Address</td>
                        <td>{!! $user->address !!}</td>
                      </tr>
                      <tr>
                        <td>Email</td>
                        <td><a href="mailto:{!! $user->email !!}">{!! $user->email !!}</a></td>
                      </tr>
                        <td>Phone Number</td>
                        <td>123-4567-890 (Landline)<br><br>555-4567-890 (Mobile)
                        </td>
                           
                      </tr>
                     
                    </tbody>
                  </table>
                  
                  <a href="#" class="btn btn-success">Sales Performance</a>
                  
                  @if (Auth::user()->type === "Manager" || Auth::user()->type === "Admin")
                  <a href="{!! route("editUser", [$user->id]) !!}" class="btn btn-primary">Edit this user</a>
                  <a href="{{{ url("staff") }}}" class="btn btn-info">Staff List</a>
                  @endif 
                </div>
              </div>
            </div>
                
            
          </div>
        </div>
      </div>
    
    
    @endif 
 
 @stop
 
 

 
