 @extends('staff.layout')

@section('title')
Staff
@stop

@section('content')
 
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <small>Staff</small>
            </h1>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-dashboard"></i>  <a href="{{{ url("dashboard") }}}">Dashboard</a>
                </li>
                <li class="active">
                    <i class="fa fa-edit"></i> Staff
                </li>
            </ol>
        </div>
    </div>
    <!-- /.row -->
    
    

</div>
<!-- /.container-fluid -->
    
    
        @if (Auth::check())
       
         
    <div class="row">
        <div class="col-sm-12">
           
              @if (count($users) == 0)
             
              <p>No USER found.</p>
              
              
              @else 
              @if (Auth::user()->type === "Admin")
              <p>
                 <a href="{!! url("staff/new") !!}" class="btn btn-success">Add</a>  
              </p>
              @endif
               <div class="panel panel-default">
                  <!-- Default panel contents -->
                  <div class="panel-heading">Staff List</div>

                    <!-- Table -->
                    <table class="table">
                    <thead> 
                        <tr> 
                            <th>#</th> 
                            <th>First Name</th> 
                            <th>Last Name</th> 
                            <th>Email</th>
                            <th>Type</th> 
                            <th>Action</th>
                        </tr> 
                    </thead> 
                     @foreach($users as $key=>$user)
                    
                     <?php $key++; ?>
                    
                    <tbody> 
                        <tr> 
                            <th scope="row">{{{ $key }}}</th> 
                            <td>{{{ $user->firstName }}}</td> 
                            <td>{{{ $user->lastName }}}</td> 
                            <td>{{{ $user->email }}}</td>
                            <td>{{{ $user->type }}}</td> 
                            <td>
                            <a href="{!! route("profile", [$user->id]) !!}" class="btn btn-success">Profile</a>
                             @if (Auth::user()->type === "Manager")
                              <a href="{!! route("editUser", [$user->id]) !!}" class="btn btn-primary">Edit</a>
                             
                              @elseif(Auth::user()->type === "Admin")
                               <a href="{!! route("editUser", [$user->id]) !!}" class="btn btn-primary">Edit</a>
                              <a href="{!! route("deleteStaff", [$user->id]) !!}" class="btn btn-danger">Delete</a>
                                
                               
                              @endif
                            </td>
                        </tr>
                    </tbody>
                    
                    @endforeach

                    </table>
                    </div>
                      
                    
                
              
              @endif
          
        </div>
      </div>
         
    
    @endif 
 
 @stop
 
 
