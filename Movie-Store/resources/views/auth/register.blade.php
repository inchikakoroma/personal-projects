@extends('auth.layout')

@section('title')
New User
@stop

@section('content')
 
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <small>New User</small>
            </h1>
            <ol class="breadcrumb">
                <li class="active">
                    @if (Auth::user()->type === "Manager" || Auth::user()->type === "Admin")
                   <i class="fa fa-dashboard"></i>  <a href="{{{ url("staff") }}}">Users</a>
                   @endif
                </li>
                <li class="active">
                    <i class="fa fa-edit"></i> New
                </li>
            </ol>
        </div>
    </div>
    <!-- /.row -->


    </div>
<!-- /.container-fluid -->
    
    
    <div class="row">
       
        <div class="col-sm-12">
    
              {!! Form::open(array('url' => ('user/new'))) !!}

              <div class="form-group">
                 {!! Form::label('firstName', 'First Name: ') !!}
                 <input type="text" class="form-control" name="firstName" value="{{ old('firstName') }}">
                 {!! $errors->first('firstName') !!}
              </div>
              
              <div class="form-group">
                 {!! Form::label('lastName', 'Last Name: ') !!}
                 <input type="text" class="form-control" name="lastName" value="{{ old('lastName') }}">
                 {!! $errors->first('lastName') !!}
              </div>
              
              
              <div class="form-group">
                
                <label>Gender</label>
                
                <label class="radio-inline">
                <input name="gender" id="gender" value="Male" checked="" type="radio">Male
                </label>
                                
                <label class="radio-inline">
                <input name="gender" id="gender" value="Female" checked="" type="radio">Female
                </label>
                
              </div>              
                            
              <div class="form-group">
                 {!! Form::label('address', 'Address: ') !!}
                 <textarea class="form-control" name="address" value="{{ old('address') }}"> </textarea>
                 {!! $errors->first('address') !!}
              </div>

              <div class="form-group">
                 {!! Form::label('email', 'Email: ') !!}
                 <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                 {!! $errors->first('email') !!}
              </div>
              
              <div class="form-group">
                 {!! Form::label('date_of_birth', 'Date Of Birth: ') !!}
                 <input type="text" id="datepicker" class="form-control" name="date_of_birth" value="{{ old('date_of_birth') }}">
                 {!! $errors->first('date_of_birth') !!}
              </div>
              
              <div class="form-group">
                {!! Form::label('storeID', 'Store: ') !!}
                {!! Form::select('storeID', [
                   '1' => 'Sydney',
                   '2' => 'Melbourne',
                    '3' => 'Brisbane']
                ) !!}
              </div>
              
             
              <div class="form-group">
                {!! Form::label('password', 'Password: ') !!}
                {!! Form::password('password', array('class' => 'form-control')) !!}
                {!! $errors->first('password') !!}
              </div>

              <div class="form-group">
                {!! Form::label('password', 'Confirm Password: ') !!}
                {!! Form::password('password_confirmation', array('class' => 'form-control')) !!}
                {!! $errors->first('password') !!}
              </div>

              <div class="form-group">
                {!! Form::label('type', 'Type: ') !!}
                {!! Form::select('type', [
                   'Staff' => 'Staff',
                   'Manager' => 'Manager',
                    'Admin' => 'Admin']
                ) !!}
              </div>
              
              
              {!! Form::submit('Create', array('class' => 'btn btn-success')) !!}
              <input type="reset" class="btn btn-info" value="Reset">
             
              {!! Form::close() !!}
              
        </div>
         
    </div>
 
 @stop
 






             